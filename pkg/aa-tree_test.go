package pkg

import (
	"reflect"
	"testing"

	"github.com/magiconair/properties/assert"
)

func TestNewNode(t *testing.T) {
	type args struct {
		data   interface{}
		weight uint
		left   *Node
		right  *Node
	}
	tests := []struct {
		name string
		args args
		want *Node
	}{
		{
			name: "New node",
			args: args{
				data:   "New tree",
				weight: 0,
				left:   &Node{},
				right:  &Node{},
			},
			want: &Node{
				Data:   "New tree",
				Weight: 0,
				rank:   0,
				left:   &Node{},
				right:  &Node{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewNode(tt.args.data, tt.args.left, tt.args.right); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewNode() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNode_Insert(t *testing.T) {
	type fields struct {
		Data   interface{}
		Weight uint
		rank   uint
		left   *Node
		right  *Node
	}
	type args struct {
		data   interface{}
		weight uint
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			node := &Node{
				Data:   tt.fields.Data,
				Weight: tt.fields.Weight,
				rank:   tt.fields.rank,
				left:   tt.fields.left,
				right:  tt.fields.right,
			}
			if err := node.Insert(tt.args.data, tt.args.weight); (err != nil) != tt.wantErr {
				t.Errorf("Insert() error = %v, wantedErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestNode_Remove(t *testing.T) {
	type fields struct {
		Data   interface{}
		Weight uint
		rank   uint
		left   *Node
		right  *Node
	}
	type args struct {
		data   interface{}
		weight uint
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			node := &Node{
				Data:   tt.fields.Data,
				Weight: tt.fields.Weight,
				rank:   tt.fields.rank,
				left:   tt.fields.left,
				right:  tt.fields.right,
			}
			if err := node.Remove(tt.args.data); (err != nil) != tt.wantErr {
				t.Errorf("Remove() error = %v, wantedErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestNode_computeRank(t *testing.T) {
	type fields struct {
		Data   interface{}
		Weight uint
		rank   uint
		left   *Node
		right  *Node
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			node := &Node{
				Data:   tt.fields.Data,
				Weight: tt.fields.Weight,
				rank:   tt.fields.rank,
				left:   tt.fields.left,
				right:  tt.fields.right,
			}
			if err := node.computeRank(); (err != nil) != tt.wantErr {
				t.Errorf("computeRank() error = %v, wantedErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestNode_skew(t *testing.T) {
	type fields struct {
		Data   interface{}
		Weight uint
		rank   uint
		left   *Node
		right  *Node
	}
	tests := []struct {
		name      string
		fields    fields
		want      *Node
		wantedErr error
	}{
		{
			name: "Ordinary tree",
			fields: fields{
				Data: "T",
				rank: 1,
				left: &Node{
					Data: "L",
					rank: 1,
					left: &Node{
						Data:  "A",
						rank:  0,
						left:  &Node{},
						right: &Node{},
					},
					right: &Node{
						Data:  "B",
						rank:  0,
						left:  &Node{},
						right: &Node{},
					},
				},
				right: &Node{
					Data:  "R",
					rank:  0,
					left:  &Node{},
					right: &Node{},
				},
			},
			want: &Node{
				Data: "L",
				rank: 1,
				left: &Node{
					Data:  "A",
					rank:  0,
					left:  &Node{},
					right: &Node{},
				},
				right: &Node{
					Data: "T",
					rank: 1,
					left: &Node{
						Data:  "B",
						rank:  0,
						left:  &Node{},
						right: &Node{},
					},
					right: &Node{
						Data:  "R",
						rank:  0,
						left:  &Node{},
						right: &Node{},
					},
				},
			},
			wantedErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			node := &Node{
				Data:   tt.fields.Data,
				Weight: tt.fields.Weight,
				rank:   tt.fields.rank,
				left:   tt.fields.left,
				right:  tt.fields.right,
			}
			res, err := node.skew()
			assert.Equal(t, err, tt.wantedErr)
			assert.Equal(t, res, tt.want)
		})
	}
}

func TestNode_split(t *testing.T) {
	type fields struct {
		Data   interface{}
		Weight uint
		rank   uint
		left   *Node
		right  *Node
	}
	tests := []struct {
		name      string
		fields    fields
		want      *Node
		wantedErr error
	}{
		{
			name: "Ordinary tree",
			fields: fields{
				Data: "T",
				rank: 1,
				left: &Node{
					Data:  "A",
					rank:  0,
					left:  &Node{},
					right: &Node{},
				},
				right: &Node{
					Data: "R",
					rank: 1,
					left: &Node{
						Data:  "B",
						rank:  0,
						left:  &Node{},
						right: &Node{},
					},
					right: &Node{
						Data:  "X",
						rank:  1,
						left:  &Node{},
						right: &Node{},
					},
				},
			},
			want: &Node{
				Data: "R",
				rank: 2,
				left: &Node{
					Data: "T",
					rank: 1,
					left: &Node{
						Data:  "A",
						rank:  0,
						left:  &Node{},
						right: &Node{},
					},
					right: &Node{
						Data:  "B",
						rank:  0,
						left:  &Node{},
						right: &Node{},
					},
				},
				right: &Node{
					Data:  "X",
					rank:  1,
					left:  &Node{},
					right: &Node{},
				},
			},
			wantedErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			node := &Node{
				Data:   tt.fields.Data,
				Weight: tt.fields.Weight,
				rank:   tt.fields.rank,
				left:   tt.fields.left,
				right:  tt.fields.right,
			}
			res, err := node.split()
			assert.Equal(t, err, tt.wantedErr)
			assert.Equal(t, res, tt.want)
		})
	}
}
