package pkg

import (
	"fmt"
)

// AA-tree
type Node struct {
	Data   interface{}
	Weight uint
	rank   uint
	left   *Node
	right  *Node
}

func NewNode(data interface{}, weight uint) *Node {
	return &Node{
		Data:   data,
		Weight: weight,
		rank:   0,
		left:   nil,
		right:  nil,
	}
}

// Insert element
func (node *Node) Insert(data interface{}, weight uint) *Node {
	switch true {
	case node.rank == 0:
		if node.right != nil {
			return node.Insert(data, weight).split().skew()
		}
		node.right = NewNode(data, weight)
		return node.right.split().skew()
	case node.rank >= weight:
		return node.right.Insert(data, weight).split().skew()
	case node.rank < weight:
		return node.left.Insert(data, weight).split().skew()
	default:
		return nil
	}
}

// Remove element
func (node *Node) Remove(data interface{}, weight uint) *Node {
	removed := node.internalFind(data, weight)
	if removed.right != nil {
		heir := removed.left
		for heir.left != nil && heir.left.left != nil {
			heir = heir.left
		}
		removed.Data = heir.left.Data
		removed.Weight = heir.left.Weight
		heir.left = nil

		// HACK: Balance subtree
		node.balanceCallback(heir)
		return node
	}
	// HACK: If node hasn't right child then it is leaf
	removed = nil
	return node
}

// Find element
func (node *Node) Find(data interface{}, weight uint) (result interface{}, resultWeight uint, err error) {
	if founded := node.internalFind(data, weight); founded != nil {
		return founded.Data, founded.Weight, nil
	}
	return nil, 0, fmt.Errorf("element with data: %v  weight: %v doesn't exist", data, weight)
}

// Internal find
func (node *Node) internalFind(data interface{}, weight uint) *Node {
	switch true {
	case node.Data == data && node.Weight == weight:
		return node
	case node.Weight >= weight:
		if node.right != nil {
			return node.right.internalFind(data, weight)
		}
	case node.Weight < weight:
		if node.left != nil {
			return node.left.internalFind(data, weight)
		}
	}
	return nil
}

// Resolve left horizontal edges
func (node *Node) skew() (result *Node) {
	if node.left == nil || node.left.rank != node.rank {
		return node
	}
	left := node.left
	node.left = left.right
	left.right = node
	return left
}

// Resolve right-double horizontal edges
func (node *Node) split() (result *Node) {
	if node.right == nil || node.right.right == nil || node.right.rank != node.right.right.rank {
		return node
	}
	node.right.rank++
	right := node.right
	node.right = right.left
	right.left = node
	return right
}

// Validate node as AA-tree
func (node *Node) Validate() []error {
	var leftErr, rightErr, internalErr []error
	if node.left != nil {
		leftErr = node.left.Validate()
	}
	if node.right != nil {
		rightErr = node.right.Validate()
	}

	// Validate node
	switch true {
	// Each node should haven't left peer child
	case node.left != nil && node.left.rank >= node.rank:
		internalErr = append(internalErr, fmt.Errorf("node: %v has left peer child: %v", node, node.left))
	// Each node should haven't two or more right peer children
	case node.right != nil && node.right.right != nil && node.rank == node.right.rank && node.rank == node.right.right.rank:
		internalErr = append(internalErr, fmt.Errorf("node: %v has right peer children: %v and child: %v", node, node.right, node.right.right))
	// Each not leafed node should have left an right children
	case node.rank > 0 && (node.right == nil || node.left == nil):
		internalErr = append(internalErr, fmt.Errorf("node: %v isn't leaf and hasn't children", node))
	// Each leaf should have rank equal to 0
	case node.left == nil && node.right == nil && node.rank != 0:
		internalErr = append(internalErr, fmt.Errorf("node: %v is leaf and hasn't rank: %v not equal to 0", node, node.rank))
	}

	return append(internalErr, append(leftErr, rightErr...)...)
}

// Balance callback traced to selected node
func (node *Node) balanceCallback(selected *Node) (result *Node) {
	switch true {
	case selected == node:
		return node.skew().split()
	case selected.Weight >= node.Weight:
		return node.right.balanceCallback(selected).skew().split()
	case selected.Weight < node.Weight:
		return node.left.balanceCallback(selected).skew().split()
	default:
		return node
	}
}

// Compute node rank
func (node *Node) computeRank() (err error) {
	leftLevel := uint(0)
	if node.left != nil {
		err = node.left.computeRank()
		if err != nil {
			return fmt.Errorf("left rank computing error: %v", err)
		}
		leftLevel = node.left.rank
	}
	rightLevel := uint(1)
	if node.right != nil {
		err = node.right.computeRank()
		if err != nil {
			return fmt.Errorf("rigth rank computing error: %v", err)
		}
		rightLevel = node.right.rank
	}

	switch true {
	case leftLevel > rightLevel:
		node.rank = leftLevel + 1
	case leftLevel < rightLevel:
		node.rank = rightLevel + 1
	default:
		node.rank = leftLevel + 1
	}

	return nil
}
